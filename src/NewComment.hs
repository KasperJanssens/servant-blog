{-# LANGUAGE DeriveGeneric #-}

module NewComment where

import           Data.Aeson.Types           (FromJSON, ToJSON)
import           GHC.Generics               (Generic)

newtype NewComment
  = NewComment {content :: String}
  deriving (Generic, Show, Read)

instance ToJSON NewComment

instance FromJSON NewComment