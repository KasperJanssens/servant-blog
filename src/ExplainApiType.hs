{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE CPP #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PolyKinds #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}

module ExplainApiType where

import Comment
import Data.String.Conversions
import Data.Text as T
import Network.HTTP.Types.Header (hContentType)
import Network.Wai.Handler.Warp (run)
import Network.Wai.Internal (requestHeaders)
import NewComment
import Servant
import Servant.API.Modifiers (FoldLenient)
import Servant.Server.Internal
import Servant.Server.Internal.Delayed (Delayed)
import Servant.Server.Internal.Router (Router, pathRouter)

data Comments = Comments

instance HasServer api context => HasServer (Comments :> api) context where
  type ServerT (Comments :> api) m = ServerT api m

  hoistServerWithContext (Proxy :: Proxy (Comments :> api)) proxyContext transformer servantApi =
    hoistServerWithContext (Proxy :: Proxy api) proxyContext transformer servantApi

  route Proxy context subServer =
    pathRouter
      "comments"
      (route (Proxy :: Proxy api) context subServer)

data CaptureCommentId = CaptureCommentId 

instance (HasServer api context) => HasServer (CaptureCommentId :> api) context where
  type ServerT (CaptureCommentId :> api) m = Int -> ServerT api m

  hoistServerWithContext (Proxy :: Proxy (CaptureCommentId :> api)) proxyContext transformer server =
   let curried = hoistServerWithContext (Proxy :: Proxy api) proxyContext transformer in let res = curried . server in res

  route Proxy context subServer =  let readFun = return . read . cs in let capt = addCapture subServer readFun  in CaptureRouter $ route (Proxy :: Proxy api) context capt 

--data ProvideNewComment = ProvideNewComment

--provideNewCommentContentTypeCheck :: DelayedIO (Either String ())
--provideNewCommentContentTypeCheck = withRequest $ \request -> do
--  let maybeContentTypeHeader = lookup hContentType $ requestHeaders request
--  case maybeContentTypeHeader of
--    Just "application/json" -> return $  Right ()
--    _ -> delayedFail err415

--instance (HasServer api context) => HasServer (ProvideNewComment :> api) context where
--  type ServerT (ProvideNewComment :> api) m = NewComment -> ServerT api m
--
--  hoistServerWithContext (Proxy :: Proxy (ProvideNewComment :> api)) proxyContext transformer server = hoistServerWithContext (Proxy :: Proxy api) proxyContext transformer . server
--
--  route Proxy context subServer = route (Proxy :: Proxy api) context $ addBodyCheck subServer provideNewCommentContentTypeCheck undefined

type ExplainApi =
  --  Comments :> Post '[JSON] String
  --    :<|>
  Comments
        :> CaptureCommentId
    :> Get '[JSON] String

explainApi :: Proxy ExplainApi
explainApi = Proxy

insertComment :: Handler String
insertComment = undefined

getComment :: Int -> Handler String
getComment id = return $ "pretend this is comment number " ++ show id

server :: Server ExplainApi
server = getComment

--insertComment
-- :<|>

serveExplainApi :: Int -> IO ()
serveExplainApi port = run port $ serve explainApi server
