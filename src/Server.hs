module Server where

import ApiType
import Comment
import Comment (fromNewComment)
import Control.Concurrent.STM.TVar (TVar, newTVar, readTVar, readTVarIO, stateTVar, writeTVar)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.STM (STM, atomically)
import Control.Monad.Trans.Reader (ReaderT, ask, asks, runReaderT)
import Cors
import Data.ByteString.Lazy.Char8 as BSLazy
import qualified Data.List as L
import qualified Data.List as List
import Data.Maybe (fromJust)
import Network.Wai.Handler.Warp (run)
import NewComment (NewComment)
import Servant

type InMemDb = ([Comment], Int)

newtype State
  = State
      { commentsInMemDb :: TVar InMemDb
      }

type AppM = ReaderT State Handler

fixedComments :: [Comment]
fixedComments = [Comment 1 "A comment", Comment 2 "Another comment"]

listComments :: AppM (ListHeaders [Comment])
listComments = do
  commentsTransactionVar <- asks commentsInMemDb
  commentsDb <- liftIO $ readTVarIO commentsTransactionVar
  let comments = fst commentsDb
  return $ addHeader (List.length comments) comments

getComment :: Int -> AppM Comment
getComment requestedId = do
  commentsTransactionVar <- asks commentsInMemDb
  commentsDb <- liftIO $ readTVarIO commentsTransactionVar
  let comments = fst commentsDb
  let maybeComment = L.find (\comment -> Comment.id comment == requestedId) comments
   in maybe (throwError err404 {errBody = BSLazy.pack $ "Could not retrieve comment with id " ++ show requestedId}) return maybeComment

insertInternal :: NewComment -> InMemDb -> (Comment, InMemDb)
insertInternal newComment curState@(comments, nextId) =
  let comment = fromNewComment nextId newComment in
  let updatedComments = List.insert comment comments in
  (comment, (updatedComments, nextId + 1))

insertComment :: NewComment -> AppM Comment
insertComment newComment = do
  commentsDbTVar <- asks commentsInMemDb
  liftIO $ atomically $ stateTVar commentsDbTVar $ insertInternal newComment

deleteInternal :: Int -> InMemDb -> (Maybe Comment, InMemDb)
deleteInternal idToDelete curState@(comments, nextId) =
  let maybeCommentToDelete = List.find (\comment -> Comment.id comment == idToDelete) comments
   in maybe
        (Nothing, curState)
        ( \commentToDelete ->
            let prunedComments = List.delete commentToDelete comments
             in (Just commentToDelete, (prunedComments, nextId))
        )
        maybeCommentToDelete

deleteComment :: Int -> AppM Comment
deleteComment idToDelete = do
  commentsDbTVar <- asks commentsInMemDb
  maybeDeletedComment <-
    liftIO $ atomically $
      stateTVar commentsDbTVar $ deleteInternal idToDelete
  maybe (throwError err404) return maybeDeletedComment

server :: ServerT WebApi AppM
server = listComments :<|> getComment :<|> insertComment :<|> deleteComment

runWithState :: State -> AppM a -> Handler a
runWithState state appM = runReaderT appM state

createInitialState :: IO State
createInitialState = do
  initialComments <- atomically $ newTVar (fixedComments, List.length fixedComments + 1)
  return $ State {commentsInMemDb = initialComments}

app :: ServerT WebApi Handler -> Application
app initedServer =
  corsConfig $ serve api initedServer

start :: Int -> IO ()
start port = do
  initialState <- createInitialState
  let initedServer = hoistServer api (runWithState initialState) server
  run port $ app initedServer
