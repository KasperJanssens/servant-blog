{-# LANGUAGE OverloadedStrings #-}

module Cors where

import qualified Network.HTTP.Types as HttpTypes
import Network.Wai (Middleware)
import Network.Wai.Middleware.Cors
  ( cors,
    corsExposedHeaders,
    corsMethods,
    corsRequestHeaders,
    simpleCorsResourcePolicy,
  )

corsConfig :: Middleware
corsConfig = cors (const $ Just policy)
  where
    policy =
      simpleCorsResourcePolicy
        { corsExposedHeaders = Just ["X-Total-Count"],
          corsMethods = [HttpTypes.methodGet, HttpTypes.methodPost, HttpTypes.methodHead, HttpTypes.methodOptions, HttpTypes.methodDelete],
          corsRequestHeaders = [HttpTypes.hContentType]
        }
