{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE InstanceSigs #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}

module Comment where

import Data.Aeson.Types (FromJSON, ToJSON)
import Data.ByteString.Lazy as BSLazy
import Data.ByteString.Lazy.Char8 as BSLazyChar8
import GHC.Generics (Generic)
import qualified NewComment
import NewComment (NewComment)
import Servant (Proxy)
import Servant.API.ContentTypes (MimeRender (..), PlainText)

data Comment
  = Comment
      { id :: Int,
        content :: String
      }
  deriving (Generic, Show, Ord, Eq)

instance ToJSON Comment

instance FromJSON Comment

toBytestring :: Comment -> ByteString
toBytestring comment =
  BSLazy.concat
    [ "I made a comment with key ",
      BSLazyChar8.pack . show . Comment.id $ comment,
      " and content ",
      BSLazyChar8.pack . content $ comment
    ]

instance MimeRender PlainText [Comment] where
  mimeRender :: Proxy PlainText -> [Comment] -> BSLazy.ByteString
  mimeRender _ comments = intercalate ";" $ fmap toBytestring comments

fromNewComment :: Int -> NewComment -> Comment
fromNewComment id newComment =
  let content = NewComment.content newComment
   in Comment {Comment.id, content}
