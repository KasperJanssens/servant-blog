{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module ApiType where

import Comment
import NewComment
import Servant

type TotalCountHeader = Header "X-Total-Count" Int

type ListHeaders v = Headers '[TotalCountHeader] v

type WebApi =
  "comments" :> Get '[JSON] (ListHeaders [Comment])
    :<|> "comments" :> Capture "id" Int :> Get '[JSON] Comment
    :<|> "comments" :> ReqBody '[JSON] NewComment :> Post '[JSON] Comment
    :<|> "comments" :> Capture "id" Int :> Delete '[JSON] Comment
 
api :: Proxy WebApi
api = Proxy
