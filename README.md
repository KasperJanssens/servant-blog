# servant-blog

This is the first part of the servant blog where we set up a really simple web server and web ui with Haskell Servant and React Admin.

You need Haskell Stack and yarn or npm to run the example. The server can be started by running stack run in the root folder. The Web UI can be started by running yarn start in the webui folder.

Full explanation can be found in the blog posts on the [website](https://propellant.tech). 