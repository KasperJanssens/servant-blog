import React from 'react';
import {Datagrid, List, TextField, Show, SimpleShowLayout, Create, required, SimpleForm, TextInput } from 'react-admin';

export const CommentsList = props => (
    <List {...props}>
        <Datagrid rowClick="show">
            <TextField source="id"/>
            <TextField source="content"/>
        </Datagrid>
    </List>
);

export const CommentsShow = props => (
    <Show {...props}>
        <SimpleShowLayout>
            <TextField source="id"/>
            <TextField source="content"/>
        </SimpleShowLayout>
    </Show>
);

export const CommentsCreate = props => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="content" validate={required()}/>
        </SimpleForm>
    </Create>
)

