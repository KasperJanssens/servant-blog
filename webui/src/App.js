import React from 'react';
import logo from './logo.svg';
import {Admin, Resource, ShowGuesser} from 'react-admin';
import './App.css';
import jsonServerProvider from "ra-data-json-server";
import {CommentsCreate, CommentsList, CommentsShow} from "./comments";
const dataProvider = jsonServerProvider('http://localhost:8082');


const App = () => (
    <Admin dataProvider={dataProvider}>
        <Resource name="comments" list={CommentsList} show={CommentsShow} create={CommentsCreate}/>
    </Admin>
);


export default App;
